# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-23 22:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AddressComponent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('long_name', models.CharField(max_length=255)),
                ('short_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='AddressComponentType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=191, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='OpenPeriod',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('open_day', models.SmallIntegerField(choices=[(0, 'Sunday'), (1, 'Monday'), (2, 'Tuesday'), (3, 'Wednesday'), (4, 'Thursday'), (5, 'Friday'), (6, 'Saturday')])),
                ('open_time', models.TimeField()),
                ('close_day', models.SmallIntegerField(choices=[(0, 'Sunday'), (1, 'Monday'), (2, 'Tuesday'), (3, 'Wednesday'), (4, 'Thursday'), (5, 'Friday'), (6, 'Saturday')])),
                ('close_time', models.TimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='The human-readable name for the place. For establishment results, this is usually the canonicalized business name.', max_length=255)),
                ('status', models.CharField(choices=[('sync', 'Sync'), ('active', 'Active'), ('inactive', 'Inactive'), ('deleted', 'Deleted')], max_length=16)),
                ('website', models.URLField(blank=True, help_text="The authoritative website for this Place, such as a business' homepage.", null=True)),
                ('formatted_address', models.CharField(help_text='The human-readable address of this place - composed of one or more address components.', max_length=255)),
                ('vicinity', models.CharField(help_text='A simplified address for the Place, including the street name, street number, and locality, but not the province/state, postal code, or country.', max_length=255)),
                ('latitude', models.DecimalField(decimal_places=6, help_text="The places' geocoded latitude.", max_digits=9)),
                ('longitude', models.DecimalField(decimal_places=6, help_text="The places' geocoded longitude.", max_digits=9)),
                ('icon', models.URLField(blank=True, help_text='URL of a suggested icon which may be displayed to the user when indicating this result on a map.', null=True)),
                ('formatted_phone_number', models.CharField(blank=True, help_text="The Place's phone number in its local format.", max_length=64, null=True)),
                ('international_phone_number', models.CharField(blank=True, help_text='Phone number in international format.', max_length=255, null=True)),
                ('rating', models.DecimalField(blank=True, decimal_places=2, help_text="Place's rating, from 1.0 to 5.0, based on user reviews.", max_digits=3, null=True)),
                ('url', models.URLField(blank=True, help_text='The official Google Place Page URL of this establishment.', null=True)),
                ('utc_offset', models.IntegerField(help_text="The number of minutes this Place's current timezone is offset from UTC.")),
                ('reference_sha1', models.CharField(blank=True, help_text="A sha1 of the reference - used for ensuring uniqueness of reference's.", max_length=40, null=True, unique=True)),
                ('reference', models.TextField(help_text='A token that can be used to query the Google Places Details service in future.')),
                ('api_id', models.CharField(help_text='Unique stable identifier denoting this place.', max_length=191, unique=True)),
                ('place_id', models.CharField(help_text='Unique stable identifier denoting this place.', max_length=191, unique=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('syncd', models.DateTimeField(auto_now_add=True, help_text="When this place was last sync'd with Google's Places API.")),
            ],
        ),
        migrations.CreateModel(
            name='PlaceType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author_name', models.CharField(max_length=255)),
                ('author_url', models.CharField(max_length=255, null=True)),
                ('rating', models.DecimalField(decimal_places=2, max_digits=3)),
                ('text', models.TextField()),
                ('reviewed', models.DateTimeField()),
                ('place', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviews', to='django_google_places.Place')),
            ],
        ),
        migrations.CreateModel(
            name='ReviewAspect',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('aspect_type', models.CharField(choices=[('appeal', 'Appeal'), ('atmosphere', 'Atmosphere'), ('decor', 'Decor'), ('facilities', 'Facilities'), ('food', 'Food'), ('overall', 'Overall'), ('quality', 'Quality'), ('service', 'Service')], max_length=32)),
                ('rating', models.SmallIntegerField(help_text="Author's rating for this particular aspect, from 0 to 3.")),
                ('review', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='aspects', to='django_google_places.Review')),
            ],
        ),
        migrations.AddField(
            model_name='place',
            name='place_types',
            field=models.ManyToManyField(help_text='Feature types describing the given place.', related_name='places', to='django_google_places.PlaceType'),
        ),
        migrations.AddField(
            model_name='openperiod',
            name='place',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='opening_periods', to='django_google_places.Place'),
        ),
        migrations.AddField(
            model_name='addresscomponent',
            name='place',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='address_components', to='django_google_places.Place'),
        ),
        migrations.AddField(
            model_name='addresscomponent',
            name='types',
            field=models.ManyToManyField(related_name='address_components', to='django_google_places.AddressComponentType'),
        ),
    ]
