from googleplaces import GooglePlaces


google_places = None  # type: GooglePlaces


def set_google_places_api_client(gp_api_client):
    global google_places
    google_places = gp_api_client
